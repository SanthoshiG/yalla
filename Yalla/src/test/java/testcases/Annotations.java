package testcases;

import com.yalla.selenium.api.base.SeleniumBase;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.AfterSuite;

public class Annotations extends SeleniumBase {
  
  public void f() {
  }
  @DataProvider(name="createData")
	public Object[][] fetchData() throws IOException {
	return utils.ReadExcel.readExcel(excelfileName);
  }
  @Parameters({"url","username","password"})
  
  //@BeforeMethod
  //@BeforeMethod(groups= {"smoke","sanity"})
  @BeforeMethod(groups="anyTest")
  //public void beforeMethod() {
  public void beforeMethod(String url,String username,String password) {
	  //startApp("chrome", "http://leaftaps.com/opentaps");
	  startApp("chrome", url);
		WebElement eleUserName = locateElement("id", "username");
		//clearAndType(eleUserName, "DemoSalesManager");
		clearAndType(eleUserName, username);
		
		WebElement elePassword = locateElement("id", "password");
		//clearAndType(elePassword, "crmsfa");
		clearAndType(elePassword, password);
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
		//WebElement eleText=locateElement("link","CRM/SFA");
		//click(eleText);
  }

  @AfterMethod(groups="anyTest")
  public void afterMethod() {
		close();
  }

  @BeforeClass
  public void beforeClass() {
  }

  @AfterClass
  public void afterClass() {
  }

  @BeforeTest
  public void beforeTest() {
  }

  @AfterTest
  public void afterTest() {
  }

  @BeforeSuite
  public void beforeSuite() {
  }

  @AfterSuite
  public void afterSuite() {
  }

}
