package com.yalla.selenium.api.base;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerDriverService;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.yalla.selenium.api.design.Browser;
import com.yalla.selenium.api.design.Element;

import utils.Report;

public class SeleniumBase extends Report implements Browser, Element{

	public RemoteWebDriver driver;
	WebDriverWait wait;
	public int i;
	@Override
	public void click(WebElement ele) {
		try {
			wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.elementToBeClickable(ele));
			ele.click();
			reportStep("The Element "+ele+" clicked","pass");
			//System.out.println("The Element "+ele+" clicked");
		} catch (StaleElementReferenceException e) {
			reportStep("The Element "+ele+" clicked","fail");
			
			throw new RuntimeException();
		}

	}
	
	//ReportStep
	public void reportStep(String dec,String status) {
		if(status.equalsIgnoreCase("pass")) {
			test.pass(dec);
		}
		else if(status.equalsIgnoreCase("fail")) {
			test.fail(dec);
		}
	}

	@Override
	public void append(WebElement ele, String data) {
		try {
			String text=ele.getText();
			text.concat(data);
			reportStep("Text is appended Successfully","pass");
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			reportStep("Text is not appended Successfully","fail");
		}
		
    
	}

	@Override
	public void clear(WebElement ele) {
		try {
			ele.clear();
			reportStep("The Element "+ele+" cleared","pass");
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			reportStep("The Element "+ele+" not cleared","fail");
		}
		// TODO Auto-generated method stub

	}

	@Override
	public void clearAndType(WebElement ele, String data) {
		try {
			ele.clear();
			ele.sendKeys(data);
			reportStep("The Data :"+data+" entered Successfully","pass");
			//System.out.println("The Data :"+data+" entered Successfully");
		} catch (ElementNotInteractableException e) {
			reportStep("The Data :"+data+" entered Successfully","fail");
			throw new RuntimeException();
		}

	}

	@Override
	public String getElementText(WebElement ele) {
		String eleText="";
		try {
			eleText= ele.getText();
			if(eleText.length()>0)
			{
				reportStep("WebElement Text Retrived","pass");
			}
	    }
		 catch (Exception e) {
			
				reportStep("WebElement Text Retrived","fail");
		 }
			return eleText;
		}

		
			
		
		// TODO Auto-generated method stub
		
		
	

	@Override
	public String getBackgroundColor(WebElement ele) {
		// TODO Auto-generated method stub
		String cssValue ="";
		try {
			cssValue = ele.getCssValue("background-color");
			reportStep("BackGround color the text", "pass");
		} catch (Exception e) {
			
			reportStep("Background color is not displayed", "fail");
			
		}
		
		return cssValue;
	}

	@Override
	public String getTypedText(WebElement ele) {
		// TODO Auto-generated method stub
		String cssValue="";
		 try {
			cssValue= ele.getCssValue("color");
			reportStep("BackGround color the text", "pass");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			reportStep("BackGround color isnot displayed", "fail");
		}
		return cssValue;
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
		
		try {
			Select dropDown=new Select(ele);
			
			dropDown.selectByVisibleText(value);
			reportStep("Dropdown selected successfully", "pass");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			reportStep("Dropdown is not selected "
					+ "successfully", "fail");
		}
		// TODO Auto-generated method stub

	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {

		try {
			Select dropDown=new Select(ele);
			dropDown.selectByIndex(index);
			reportStep("Selected by Index", "pass");
		} catch ( NoSuchElementException e) {
			reportStep("Not Selected by Index", "fail");
			throw new RuntimeException();
		}
		// TODO Auto-generated method stub

	}

	@Override
	public void selectDropDownUsingValue(WebElement ele, String value) {
		// TODO Auto-generated method stub
		try {
			Select ddpItem=new Select(ele);
			ddpItem.selectByValue(value);
			reportStep("Dropdown selected with value", "pass");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			reportStep("DropDown not selected ","fail");
		}

	}

	@Override
	public boolean verifyExactText(WebElement ele, String expectedText) {
		try {
			if(ele.getText().equals(expectedText)) {
				reportStep("WebElement Text Retrived","pass");
				return true;
			}
		
		} catch (WebDriverException e) {
			reportStep("WebElement Text Retrived","fail");
		} 

		return false;
		// TODO Auto-generated method stub
	}

	@Override
	public boolean verifyPartialText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub
		try {
		if(ele.getText().contains(expectedText)) {
			reportStep("WebElement Text Retrived","pass");
			return true;
			
		}
		
	} catch (WebDriverException e) {
			reportStep("WebElement Text Retrived","fail");
		
		
		}
		return false;
		
	}
		

	@Override
	public boolean verifyExactAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub
		try {
		
		if(ele.getAttribute(attribute).equalsIgnoreCase(value)){
			reportStep("Verified Exact Attribute","pass");
			return true;
		}
		
	} catch (WebDriverException e) {
				reportStep("Verified Exact Attribute","fail");
				
			}
		return false;
			}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub
		try {
		if(ele.getAttribute(attribute).contains(value)){
			reportStep("Verified Exact Attribute","pass");
		}
		}
		catch (WebDriverException e) {
				reportStep("Verified Exact Attribute","fail");
			
			}

	}

	@Override
	public boolean verifyDisplayed(WebElement ele) {
		try{
			if(ele.isDisplayed()) {
		
			reportStep("Element is displayed","pass");
			return true;
		}
		}
		catch (WebDriverException e) {
				reportStep("Element is notdisplayed","fail");
				
		}
		return false;
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean verifyDisappeared(WebElement ele) {
		if(!ele.isDisplayed()) {
			reportStep("Element is disappeared","pass");
			return true;
		}
			else {
				reportStep("Element is displayed","fail");
				return false;
		}
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean verifyEnabled(WebElement ele) {
		// TODO Auto-generated method stub
		try {
		if(ele.isEnabled()) {
			reportStep("Button is Enabled","pass");
			return true;
		}
		}
		catch (WebDriverException e) {
				reportStep("Button is disabled","fail");
				
		}
		return false;
	}
	
	

	@Override
	public boolean verifySelected(WebElement ele) {
		// TODO Auto-generated method stub
		if(ele.isSelected()) {
			reportStep("Element is selected","pass");
			return true;
		}
			else {
				reportStep("Element is not selected","fail");
				return false;
		}
		
	}

	@Override
	public void startApp(String url) {
		// TODO Auto-generated method stub
		
		try {
			System.setProperty("webdriver.chrome.driver",
					"./drivers/chromedriver.exe");
			driver = new ChromeDriver();
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
			driver.get(url);
			reportStep("Launched Chrome Browser Successfully","pass");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			reportStep("Chrome Browser is not Launched","fail");
		}
		

	}

	@Override
	public void startApp(String browser, String url) {
		try {
			if(browser.equalsIgnoreCase("chrome")) {
				System.setProperty("webdriver.chrome.driver",
						"./drivers/chromedriver.exe");
				driver = new ChromeDriver();
				
			} else if(browser.equalsIgnoreCase("firefox")) {
				System.setProperty("webdriver.gecko.driver",
						"./drivers/geckodriver.exe");
				driver = new FirefoxDriver();
				
			} else if(browser.equalsIgnoreCase("ie")) {
				System.setProperty("webdriver.ie.driver",
						"./drivers/IEDriverServer.exe");
				driver = new InternetExplorerDriver();
				
			}
			driver.navigate().to(url);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			reportStep(" Driver Launched Succcessfully", "pass");
		} catch (Exception e) {
			reportStep("Browser not launched", "fail");
			throw new RuntimeException();
		}

	}

	@Override
	public WebElement locateElement(String locatorType, String value) {
		try {
			switch(locatorType.toLowerCase()) {
			case "id": return driver.findElementById(value);
			case "name": return driver.findElementByName(value);
			case "class": return driver.findElementByClassName(value);
			case "link": return driver.findElementByLinkText(value);
			case "xpath": return driver.findElementByXPath(value);
			}
		} catch (NoSuchElementException e) {
			reportStep("The Element with locator:"+locatorType+" Not Found with value: "+"value","fail");
			throw new RuntimeException();
		}
		return null;
	}

	@Override
	public WebElement locateElement(String value) {
		// TODO Auto-generated method stub
		try {
			driver.findElementById(value);
			reportStep("LocatedElement successfully", "pass");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			reportStep("LocatedElement unsuccessfully", "fail");
		}
		return null;
	}

	@Override
	public List<WebElement> locateElements(String type, String value) {
		try {
			switch(type.toLowerCase()) {
			case "id": return driver.findElementsById(value);
			case "name": return driver.findElementsByName(value);
			case "class": return driver.findElementsByClassName(value);
			case "link": return driver.findElementsByLinkText(value);
			case "xpath": return driver.findElementsByXPath(value);
			}
		} catch (NoSuchElementException e) {
			
			reportStep("The Element with locator","fail");
			throw new RuntimeException();
		}
		return null;
	}

	@Override
	public void switchToAlert() {
		// TODO Auto-generated method stub
		try {
			driver.switchTo().alert();
			reportStep("Switched to Alert Successfully","pass");
		} catch (NoAlertPresentException e) {
			reportStep("Unable to Switch to Alert", "fail");
		}

	}

	@Override
	public void acceptAlert() {
		try {
			driver.switchTo().alert().accept();
			reportStep("Alert Accepted Successfully","pass");
			
		} catch (NoAlertPresentException e) {
			// TODO Auto-generated catch block
			reportStep("Unable to Accept the Alert","fail");
		}


	}

	@Override
	public void dismissAlert() {
		// TODO Auto-generated method stub
		try {
			driver.switchTo().alert().dismiss();
			reportStep("Alert is not accepted","pass");
		} catch (NoAlertPresentException e) {
			// TODO Auto-generated catch block
		reportStep("Unable to Dismiss the Alert","fail");
		}

	}

	@Override
	public String getAlertText() {
		// TODO Auto-generated method stub
		try {
			driver.switchTo().alert().getText();
			reportStep("Alert Text Retriewed","pass");
		} catch (NoAlertPresentException e) {
			// TODO Auto-generated catch block
			reportStep("Unable to retriew the Data ","fail");
		}
		return null;
	}

	@Override
	public void typeAlert(String data) {
		// TODO Auto-generated method stub
		
		try {
			driver.switchTo().alert().sendKeys(data);
			driver.switchTo().alert().getText();
			driver.switchTo().alert().accept();
			reportStep("Entered Data and accepted the Alert Successfully","pass");
		} catch (NoAlertPresentException e) {
			// TODO Auto-generated catch block
			reportStep("Unable to EnterData and accept Alert","fail");
		}

	}

	@Override
	public void switchToWindow(int index) {
		try {
			Set<String> windowSet1=driver.getWindowHandles();
			List<String> listwindowSet=new ArrayList<>(windowSet1);
			String noWindows=listwindowSet.get(index);
			driver.switchTo().window(noWindows);
			reportStep("SwitchedWindowSuccessfully","pass");
		} catch (NoSuchWindowException e) {
		reportStep("The Window with Index: "+ index +" is not found","fail");
		throw new RuntimeException();
		}


		// TODO Auto-generated method stub

	}

	@Override
	public void switchToWindow(String title) {
		// TODO Auto-generated method stub

	}

	@Override
	public void switchToFrame(int index) {
		// TODO Auto-generated method stub
		try {
			driver.switchTo().frame(index);
			reportStep("switch to frame successfully","pass");
		} catch (java.util.NoSuchElementException  e) {
			// TODO Auto-generated catch block
			reportStep("The Frame name with "+index+"is not found","fail");
		}


	}

	@Override
	public void switchToFrame(WebElement ele) {
		// TODO Auto-generated method stub
		

	}

	@Override
	public void switchToFrame(String idOrName) {
		// TODO Auto-generated method stub
		try {
			driver.switchTo().frame(idOrName);
			reportStep("switched to frame", "pass");
		} catch (java.util.NoSuchElementException  e) {
			// TODO Auto-generated catch block
			reportStep("The Frame name with "+idOrName+"is not found","fail");
		}

	}

	@Override
	public void defaultContent() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean verifyUrl(String url) {
		// TODO Auto-generated method stub
		try {
			if(driver.getCurrentUrl().contains(url)) {
				reportStep("Url of the page is verified Successfully","pass");
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			reportStep("unable to reterview URL","fail");
		}
		return false;
	}

	@Override
	public boolean verifyTitle(String title) {
		// TODO Auto-generated method stub
		try {
			if(driver.getTitle().contains(title))
			{
			reportStep("Title Of the Page Verified Successfully","pass");
			return true;	
		    }		
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			reportStep("Unable to get Title ","fail");
		}
		return false;
		}

	@Override
	public void takeSnap()  {
		try {
			File src=driver.getScreenshotAs(OutputType.FILE);
			File desc=new File("./snaps/snap"+i+".png");
			FileUtils.copyFile(src, desc);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			reportStep("unable to take screenshot","fail");
		}
		
		i++;
		// TODO Auto-generated method stub

	}

	@Override
	public void close() {
		// TODO Auto-generated method stub
		try {
			driver.close();
			reportStep("Browser closed Successfully","pass");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			reportStep("Unable to close the Browser","fail");
		}

	}

	@Override
	public void quit() {
		// TODO Auto-generated method stub
		driver.quit();

	}

}
