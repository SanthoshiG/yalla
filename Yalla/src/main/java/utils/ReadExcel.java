package utils;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcel   {
	public static Object[][] readExcel(String excelfileName) throws IOException {
	XSSFWorkbook wbook=new XSSFWorkbook("./data/"+excelfileName+".xlsx");
	//Open Sheet excel index starts with 0
	XSSFSheet sheet=wbook.getSheetAt(0);
	//Get Row count ,by default getLastRowNum ignores header and start counting
	
	int rowCount=sheet.getLastRowNum();
	System.out.println("row count"+rowCount);
	//getrow(0) in order to get column names count 
	//and header wont have null values
	int colCount=sheet.getRow(0).getLastCellNum();
	System.out.println("col count"+colCount);
	Object[][] data=new Object[rowCount][colCount];
	//as row header is zero we should initalize 1 
	for (int i=1;i<=rowCount;i++) {
		//it will indicates to particular row
		XSSFRow row=sheet.getRow(i);
		for(int j=0;j<colCount;j++) {
			//it will move to particular celll
			XSSFCell cell=row.getCell(j);
			String StringCellValue=cell.getStringCellValue();
			data[i-1][j]=StringCellValue;
			System.out.println(StringCellValue);
			
			
		}
		
		
	}
	return data;
	}
}
