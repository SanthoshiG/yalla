package utils;

import java.io.IOException;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;


import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;

import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class Report {

	static ExtentHtmlReporter reporter;
	static ExtentReports extent;
	public ExtentTest test;
	public String testCaseName,testCaseDec,author,catergory;
	public static String excelfileName;
	@BeforeSuite(groups="anyTest")
	public void startReport() {
		reporter=new ExtentHtmlReporter("./reports/result.html");
		//Not to override the TC reports
	    reporter.setAppendExisting(true);
		extent=new ExtentReports(); 
		//Attach the report to the Template
		extent.attachReporter(reporter);
	
	}
	@BeforeMethod(groups="anyTest")
	public void LearnReport() throws IOException {
	
		test=extent.createTest(testCaseName,testCaseDec);
		test.assignAuthor(author);
		test.assignCategory(catergory);

		//test1.fail("unable to Login",MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/snap1.png").build());
	
	}

	
	@AfterSuite(groups="anyTest")
	 public void endReport() {
		extent.flush();
		
	}
}
