package testcases;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
public class EditSLead extends Annotations {
	@BeforeTest(groups="sanity")
	public void setData() {
		testCaseName="Edit Lead";
		testCaseDec="Editing the Lead";
		author="Santhoshi";
		catergory="Regression";
		excelfileName="dpEdit";
	}
	//@Test(dependsOnMethods= {"testcases.CreateLead.CreateL"})
	//@Test(timeOut=10000)
	//@Test(groups="sanity")
	//@Test(groups="sanity",dependsOnGroups="smoke")
	@Test (groups="smoke",dataProvider="createData")
	public void editLead(String Phone) throws InterruptedException {
		click(locateElement("link", "CRM/SFA"));
		click(locateElement("link", "Leads"));
		click(locateElement("link", "Find Leads"));
		click(locateElement("xpath","//span[text()='Phone']"));
		clearAndType(locateElement("name", "phoneNumber"), Phone); 
	    click(locateElement("xpath","//button[text()='Find Leads']"));
	 /*   Thread.sleep(1000);
	    click(locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a"));
	    click(locateElement("link", "Edit"));
	    clearAndType(locateElement("id","updateLeadForm_companyName"), "TCS");
	    click(locateElement("name", "submitButton"));*/
	
	
	}

}
