package testcases;


import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.yalla.selenium.api.base.SeleniumBase;

public class LearnFrameAlert extends SeleniumBase {
	@Test
	public void frameAlert() {
	startApp("chrome","https://www.w3schools.com/js//tryit.asp?filename=tryjs_prompt");

	//Switch to frame where element interacting
    switchToFrame("iframeResult");
    
	WebElement ButtonTry=locateElement("xpath","//button[text()='Try it']");
	click(ButtonTry);
	
	//switch to alert window
	switchToAlert();
	typeAlert("OmSriSaiRam");
	String alertMsg=locateElement("xpath","//p[@id='demo']").getText();
	
	
	
	System.out.println(alertMsg);

	if(alertMsg.contains("Sai")) {
		System.out.println("Enter Name is verified");
	}
	else {
		System.out.println("Invalid Name");

	}

	}

}


