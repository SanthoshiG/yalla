package testcases;

import org.junit.Test;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;

import com.yalla.selenium.api.base.SeleniumBase;

public class MergeLead extends Annotations {
	@BeforeClass
	public void setData() {
		testCaseName="Merge Lead";
		testCaseDec="Merging the Lead";
		author="Santhoshi";
		catergory="Regression";
	}
	@Test
	
	public void mergeLead() throws InterruptedException  {
		
		//Login
		//startApp("chrome", "http://leaftaps.com/opentaps");
		//WebElement eleUserName = locateElement("id", "username");
		//clearAndType(eleUserName, "DemoSalesManager");
		//WebElement elePassword = locateElement("id", "password");
		//clearAndType(elePassword, "crmsfa");
		//WebElement eleLogin = locateElement("class", "decorativeSubmit");
		//click(eleLogin);
		
		
		//Click crm/sfa link
		//WebElement eleCRM=locateElement("link","CRM/SFA");
		//click(eleCRM);
		click(locateElement("link", "CRM/SFA"));
		
	    //Click Leads link
		WebElement eleLeads=locateElement("xpath","//a[text()='Leads']");
		click(eleLeads);
		
		
		Thread.sleep(3000);
		//Click Merge leads
		WebElement eleMerge=locateElement("xpath","//a[text()='Merge Leads']");
		click(eleMerge);
	
		//Click on Icon near From Lead
		Thread.sleep(3000);
		WebElement eleIcon=locateElement("xpath","//table[@id='widget_ComboBox_partyIdFrom']/following-sibling::a/img");
		click(eleIcon);
		
		
		//Switch to New Window
		switchToWindow(1);
		
		
		//Enter the Lead ID
		WebElement eleLeadno=locateElement("xpath","//input[@name='id']");
		clearAndType(eleLeadno,"10557");
		
		
		//Click Find Leads
	WebElement eleFL=locateElement("xpath","//button[text()='Find Leads']");
	click(eleFL);
	
		Thread.sleep(3000);
		WebElement eleCal=locateElement("xpath","(//table[@class='x-grid3-row-table']//div//a)[1]");
		click(eleCal);
		
		
		//Switch back to primary window
		switchToWindow(0);
		
		
		//Click on Icon near To Lead
		WebElement eleCal2=locateElement("xpath","//table[@id='widget_ComboBox_partyIdTo']/following-sibling::a/img");
		click(eleCal2);
	
		
		//switch to new Window
		switchToWindow(1);
		
		
		
		Thread.sleep(2000);
		//enter the lead ID
		WebElement eleLead1=locateElement("xpath","//input[@name='id']");
		clearAndType(eleLead1,"10625");
		
		WebElement eleButton1=locateElement("xpath","//button[text()='Find Leads']");
		click(eleButton1);
		
		
		Thread.sleep(1000);
		WebElement eleSearch=locateElement("xpath","(//table[@class='x-grid3-row-table']//a)[1]");
		click(eleSearch);
		
		
		Thread.sleep(2000);
		//Switch back to primary window
		switchToWindow(0);
		
		
		//click on Merge
		WebElement eleMer=locateElement("xpath","//a[text()='Merge']");
		click(eleMer);
	
		
		//switch to alert
		acceptAlert();
		
		
		
		//Click Find Leads
		WebElement eleBB=locateElement("xpath","//a[text()='Find Leads']");
		click(eleBB);
		
		
		
		WebElement eleBID=locateElement("xpath","//input[@name='id']");
		clearAndType(eleBID,"10557");
		
		
		WebElement eleButt3=locateElement("xpath","//button[text()='Find Leads']");
		click(eleButt3);
		
		Thread.sleep(3000);
		WebElement ele=locateElement("xpath","//div[@class='x-paging-info']");
		
		String Name=ele.getText();
		if(Name.contains("No records to display")) {
			System.out.println("Records Merged Successfully");
		}
		else
		{
			System.out.println("Records are not Merged");
		}
		
		
		}

	
		
	
		
		
		
		

	}


