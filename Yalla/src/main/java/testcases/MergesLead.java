package testcases;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
public class MergesLead extends Annotations {
	@BeforeClass(groups="regression")
	public void setData() {
		testCaseName="Merge Lead";
		testCaseDec="Merging the Lead";
		author="Santhoshi";
		catergory="Regression";
	}
	//@Test(enabled=false)
	@Test(groups="regression")
	public void mergeLead() throws InterruptedException {
		click(locateElement("link", "CRM/SFA"));
		click(locateElement("link", "Leads"));
		click(locateElement("link", "Find Leads"));
		click(locateElement("link", "Merge Leads")); 
		click(locateElement("xpath", "//img[@alt='Lookup']"));
		switchToWindow(1);
		clearAndType(locateElement("xpath", "//input[@name='firstName']"), "gopi");
		click(locateElement("xpath","//button[text()='Find Leads']"));
	    Thread.sleep(1000);
	    String text = locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a").getText();
	    clickWithNoSnap(locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a"));
	    switchToWindow(0);
	    click(locateElement("xpath", "(//img[@alt='Lookup'])[2]"));
		switchToWindow(1);
		clearAndType(locateElement("xpath", "//input[@name='firstName']"), "babu");
		click(locateElement("xpath","//button[text()='Find Leads']"));
	    Thread.sleep(1000);
	    clickWithNoSnap(locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a"));
	    switchToWindow(0);
	    clickWithNoSnap(locateElement("xpath", "//a[text()='Merge']"));
	    acceptAlert();
	    click(locateElement("link", "Find Leads"));
	    clearAndType(locateElement("xpath", "//input[@name='id']"), text);
	    click(locateElement("xpath","//button[text()='Find Leads']"));
	    verifyExactText(locateElement("class","x-paging-info"), "No records to display");
	}
	
}


