package testcases;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class EditLead extends Annotations{
	@Test
	public  void edit() throws InterruptedException {
		
		
		//Click crm/sfa link
		WebElement ele1=locateElement("link","CRM/SFA");
		ele1.click();
		
		//driver.findElementByLinkText("CRM/SFA").click();
		//Click Leads link
		WebElement elLeads=locateElement("xpath","//a[text()='Leads']");
		elLeads.click();
		
		Thread.sleep(2000);
		//Click Find leads
		WebElement eFindLeads=locateElement("xpath","//a[text()='Find Leads']");
		eFindLeads.click();
    	//driver.findElementByXPath("//a[text()='Find Leads']").click();
		//Enter first name
		//WebElement eFirstName=locateElement("xpath","(//input[@name='firstName'])[3]");
		//clearAndType(eFirstName,"Shri Sai Ram");
       // driver.findElementByXPath("(//input[@name='firstName'])[3]").sendKeys("Shri Sai Ram");		
        //Click Find leads button
		WebElement ebutton=locateElement("xpath","//button[text()='Find Leads']");
		ebutton.click();
       // driver.findElementByXPath("//button[text()='Find Leads']").click();
        //Click on first resulting lead
        Thread.sleep(3000);
        WebElement eleClick=locateElement("xpath","(//table[@class='x-grid3-row-table']//a)[1]");
        eleClick.click();
        //driver.findElementByXPath("(//table[@class='x-grid3-row-table']//a)[1]").click();
        //String winTitle=driver.getTitle();
        //System.out.println("WebPage Title :"+winTitle);
        verifyTitle("View Lead | opentaps CRM");
       // String ExceptedTitle="View Lead | opentaps CRM";
        //if(winTitle.contentEquals(ExceptedTitle)) {
        	//System.out.println("Page title verified");
        	  //Click Edit
        
        	//driver.findElementByXPath("//a[text()='Edit']").click();
        	WebElement eleEditClick=locateElement("xpath","//a[text()='Edit']");
        	eleEditClick.click();
        	//Change the company name
        	WebElement eleCompanyName=locateElement("xpath","//input[@id='updateLeadForm_companyName']");
        	clearAndType(eleCompanyName,"Amazon");
        	//driver.findElementByXPath("//input[@id='updateLeadForm_companyName']").clear();
        	//WebElement eleUpdateName=locateElement("xpath","//input[@id='updateLeadForm_companyName']");
        	
        	
        	//driver.findElementByXPath("//input[@id='updateLeadForm_companyName']").sendKeys("Amazon");
        	//Click on UpdateButton
        	WebElement upButton=locateElement("xpath","(//input[@name='submitButton'])[1]");
        	click(upButton);
        	//driver.findElementByXPath("(//input[@name='submitButton'])[1]").click();
        	//WebElement updateName=driver.findElementByXPath("//span[@id='viewLead_companyName_sp']");
        	//String EditName=updateName.getText();
        	//System.out.println("updated"+EditName);
        	//if(EditName.contains("Amazon")) {
        	//	System.out.println("Updated the Company Name Successfully");
        	//}
        	//else {
        	//	System.out.println("Company Name Not Updated");
        	//}
        
        //}
       // else {
       // 	System.out.println("Page title didnt Matched");
      //  }
        
        //close the Browser
        driver.close();
      
        
        
        
        
        
        
	}
	
	

}
