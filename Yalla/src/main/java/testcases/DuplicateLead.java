package testcases;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class DuplicateLead {
	public static void main(String args[]) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.navigate().to("http://leaftaps.com/opentaps");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
		//Enter the User Name
		driver.findElementById("username").sendKeys("DemoSalesManager");
		//Enter the Password
		driver.findElementById("password").sendKeys("crmsfa");
		//click Login
		driver.findElementByClassName("decorativeSubmit").click();
		//Click crm/sfa link
		driver.findElementByLinkText("CRM/SFA").click();
		//click Leads
		driver.findElementByLinkText("Leads").click();
		//Click Find leads
		driver.findElementByLinkText("Find Leads").click();
		//Click on Email
		driver.findElementByXPath("(//span[@class='x-tab-strip-text '])[3]").click();
		//Enter Email
		driver.findElementByName("emailAddress").sendKeys("santhoshigodavarthi@gmail.com");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		//click First Resulting lead
		Thread.sleep(3000);
		driver.findElementByXPath("(//table[@class='x-grid3-row-table']//td//a)[1]").click();
		
		//capture Name of first Resulting lead
		
		String name=driver.findElementById("viewLead_firstName_sp").getText();
		System.out.println("Firstname to get Duplicate :"+name);
		//Click Duplicate Lead
		
		driver.findElementByLinkText("Duplicate Lead").click();
		//verify Title of the page
		if(driver.getTitle().contains("Duplicate Lead")) {
			//Click Create Lead
			driver.findElementByXPath("//input[@class='smallSubmit']").click();
			String Name2=driver.findElementById("viewLead_firstName_sp").getText();
			if(Name2.equals(name)) {
				System.out.println("Duplicate Lead created successfully");
			}
			else {
				System.out.println("Page title didnt match ");
			}
		driver.close();
		}
	}

}
