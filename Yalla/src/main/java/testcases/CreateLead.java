package testcases;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;



public class CreateLead extends Annotations{
	@BeforeTest(groups="smoke")
	public void SetData() {
		testCaseName="CreateLead";
		testCaseDec="Create New Lead";
		author="Santhoshi";
		catergory="Smoke";
		excelfileName="dpCreateLead";
	}
	
	//@Test(invocationCount=1)
	//@Test (groups="smoke")
	@Test (groups="smoke",dataProvider="createData")
	public void CreateL(String Cname,String fname,String lname) {
		
		WebElement eleText=locateElement("link","CRM/SFA");
		click(eleText);
		takeSnap();
		WebElement eleCreateLeadTab=locateElement("link","Create Lead");
		//WebElement eleCreateLeadTab=locateElement("link","Create Lead2");
	    click(eleCreateLeadTab);
	    takeSnap();
		WebElement eleCompanyName=locateElement("id","createLeadForm_companyName");
		//clearAndType(eleCompanyName,"CholaMS");
		clearAndType(eleCompanyName,Cname);
		
		WebElement eleFirstName=locateElement("id","createLeadForm_firstName");
		//clearAndType(eleFirstName,"Santhoshi");
		clearAndType(eleFirstName,fname);
		
		WebElement eleLastName=locateElement("id","createLeadForm_lastName");
		//clearAndType(eleLastName,"Godavarthi");
		clearAndType(eleLastName,lname);

		//WebElement eleDD=locateElement("xpath","//select[@id='createLeadForm_dataSourceId']");
		//selectDropDownUsingIndex(eleDD,3);
		
		
		
		WebElement eleButton=locateElement("xpath","//input[@name='submitButton']");
		click(eleButton);
		
	

		
	}
	//@DataProvider(name="createData")
	//public Object[][] fetchData() throws IOException {
	//return utils.ReadExcel.readExcel(excelfileName);
		
		//Object[][] data=new Object[2][3];
		//data[0][0]="NTT DATA";
		//data[0][1]="Jagadeswari";
		//data[0][2]="Garakipati";
		//data[1][0]="Test Leaf";
		//data[1][1]="Chandhu";
		//data[1][2]="Garakipati";
		//return data;
		
		
		
	}
	
	


